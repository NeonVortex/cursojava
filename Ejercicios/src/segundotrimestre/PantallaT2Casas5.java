package segundotrimestre;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PantallaT2Casas5 {

	private JFrame frame;
	private JTextField txt;
	private JLabel lblResultado;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaT2Casas5 window = new PantallaT2Casas5();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public PantallaT2Casas5() {
		initialize();
	}

	
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(111, 111, 111));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNumero = new JLabel("Numero");
		lblNumero.setFont(new Font("TimesRoman", Font.PLAIN, 20));
		lblNumero.setForeground(new Color(0, 0, 0));
		lblNumero.setBounds(108, 34, 70, 25);
		frame.getContentPane().add(lblNumero);
		
		txt = new JTextField();
		txt.setBounds(217, 38, 116, 22);
		frame.getContentPane().add(txt);
		txt.setColumns(10);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int numero = Integer.parseInt(txt.getText());
				
				if (numero%2==0)
					lblResultado.setText("Es par");
				else lblResultado.setText("Es impar");
				
			}
		});
		btnCalcular.setFont(new Font("TimesRoman", Font.BOLD, 16));
		btnCalcular.setBounds(92, 123, 112, 31);
		frame.getContentPane().add(btnCalcular);
		
		lblResultado = new JLabel("Resultado");
		lblResultado.setOpaque(true);
		lblResultado.setBackground(new Color(255, 111, 111));
		lblResultado.setFont(new Font("TimesRoman", Font.PLAIN, 20));
		lblResultado.setForeground(new Color(0, 0, 0));
		lblResultado.setBounds(235, 121, 88, 31);
		frame.getContentPane().add(lblResultado);
	}

}
