package modulo1;

public class Ejercicio2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Nombre \t\t Apellido \t Email \t\t\t\t\t Materia");
		System.out.println("------ \t\t -------- \t ----- \t\t\t\t\t -------");
		System.out.println("\n\n\nGabriel \t Casas \t\t gcasas1972@gmail.com \t\t\t Computadoras de Aeronaves");
		System.out.println("Waltre \t\t Stahler \t walterstahler@gmail.com \t\t Sistemas de Control de Vuelo");
		System.out.println("Bruno \t\t Cimalando \t profesor_cimalando@yahoo.com.ar \t Emprendimiento Produccion y Desarrollo Local");
		System.out.println("Jose \t\t Piatti \t lu1ebi@yahoo.com.ar \t\t\t Sistemas de Comunicaciones y Microondas");
	}

}
