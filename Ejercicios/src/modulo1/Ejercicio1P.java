package modulo1;
import java.util.Scanner;
import java.awt.EventQueue;

import javax.swing.JFrame;

public class Ejercicio1P {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Scanner s= new Scanner(System.in);
		System.out.println("Ingresar valor");
		int a= s.nextInt();
		if (a%2 == 0){
			System.out.println("Es numero par");
		} else{ 
		System.out.println("Es numero impar");
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio1P window = new Ejercicio1P();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio1P() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
