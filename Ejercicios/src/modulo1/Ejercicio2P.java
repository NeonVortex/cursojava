package modulo1;
import java.util.Scanner;
import java.awt.EventQueue;

import javax.swing.JFrame;

public class Ejercicio2P {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Scanner s= new Scanner(System.in);
		System.out.println("Ingresar posicion en numeros");
		int a= s.nextInt();
		if (a == 1){
			System.out.println("Medalla dorada");
		} else{
			if (a==2){
				System.out.println("Medalla de plata");
			} else{
				if (a==3){
				System.out.println("Medalla de bronce");
				} else {
					System.out.println("No alcanzo el podio, siga participando");
				}
			}
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio2P window = new Ejercicio2P();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio2P() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
